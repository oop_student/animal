/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.animal;

/**
 *
 * @author DELL
 */
public abstract class Poultry extends Animal{

    public Poultry(String name, int numberOfleg) {
        super(name, numberOfleg);
    }
    
    public abstract void fly();
}
