/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.animal;

/**
 *
 * @author DELL
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        Crocodile cr1 = new Crocodile("Keaw");
        cr1.crawl();
        Snake sn1 = new Snake("lam");
        sn1.crawl();
        Dog d1 = new Dog("dum");
        d1.eat();
        Cat c1 = new Cat("Meow");
        c1.walk();
        Fish f1 = new Fish("tong");
        f1.walk();
        f1.swim();
        Crab cb1 = new Crab("putai");
        cb1.swim();
        Bat b1 = new Bat("bang");
        b1.fly();
        Bird br1 = new Bird("Jib");
        br1.fly();
        System.out.println("h1 is animal ? "+ (h1 instanceof Animal));
        System.out.println("cr1 is animal ? "+ (cr1 instanceof Animal));
        System.out.println("sn1 is animal ? "+ (sn1 instanceof Animal));
        System.out.println("d1 is animal ? "+ (d1 instanceof Animal));
        System.out.println("c1 is animal ? "+ (c1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal ? "+ (f1 instanceof AquaticAnimal));
        System.out.println("cb1 is AquaticAnimal ? "+ (cb1 instanceof AquaticAnimal));
        System.out.println("b1 is Animal ? "+ (b1 instanceof Animal));
        System.out.println("br1 is Animal ? "+ (br1 instanceof Animal));
    }
            
}
