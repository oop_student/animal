/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.animal;

/**
 *
 * @author DELL
 */
public class Cat extends LandAnimal{
    private String nickname;

    public Cat(String nickname) {
        super("Cat", 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Snake: " + nickname + " run");
    }

    @Override
    public void eat() {
           System.out.println("Snake: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake: " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake: " + nickname + " sleep");
    }
    
}
